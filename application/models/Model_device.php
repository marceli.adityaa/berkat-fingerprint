<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_device extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
            $result = $this->db->get('device');
            if ($result->num_rows() > 0) {
                return $result->row_array();
            } else {
                return false;
            }
        } else {
            $this->db->order_by('id');
            $this->db->order_by('is_active', 'desc');
            $result = $this->db->get('device');
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return false;
            }
        }
    }

    public function get_active_device()
    {
        $this->db->where('is_active', 1);
        $this->db->order_by('name');
        $result = $this->db->get('device');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function is_exist($id){
        $this->db->where('device_id', $id);
        $result = $this->db->get('user');
        if($result->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function insert($data)
    {
        return $this->db->insert('device', $data);
    }

    public function update($data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update('device', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('device');
    }
}