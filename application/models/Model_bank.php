<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_bank extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        if (!empty($id)) {
            $this->db->where('account_id', $id);
            $result = $this->db->get('bank_account');
            if ($result->num_rows() > 0) {
                return $result->row_array();
            } else {
                return false;
            }
        } else {
            $this->db->order_by('account_status', 'desc');
            $this->db->order_by('account_id');
            $result = $this->db->get('bank_account');
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return false;
            }
        }
    }

    public function get_active_bank()
    {
        $this->db->where('account_status', 1);
        $this->db->order_by('account_bank');
        $this->db->order_by('account_name');
        $result = $this->db->get('bank_account');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_active_group_bank()
    {
        $this->db->where('account_status', 1);
        $this->db->order_by('account_bank');
        $this->db->group_by('account_bank');
        $result = $this->db->get('bank_account');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function is_exist($id){
        $this->db->where('account_from', $id);
        $this->db->or_where('account_to', $id);
        $result = $this->db->get('purchase_repayment');
        if($result->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function insert($data)
    {
        return $this->db->insert('bank_account', $data);
    }

    public function update($data, $id)
    {
        $this->db->where('account_id', $id);
        return $this->db->update('bank_account', $data);
    }

    public function delete($id)
    {
        $this->db->where('account_id', $id);
        return $this->db->delete('bank_account');
    }
}