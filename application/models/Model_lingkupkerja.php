<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_lingkupkerja extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
            $result = $this->db->get('lingkup_kerja');
            if ($result->num_rows() > 0) {
                return $result->row_array();
            } else {
                return false;
            }
        } else {
            $this->db->order_by('id');
            $this->db->order_by('status', 'desc');
            $result = $this->db->get('lingkup_kerja');
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return false;
            }
        }
    }

    public function get_active_lingkupkerja()
    {
        $this->db->where('status', 1);
        $this->db->order_by('lingkup_kerja');
        $result = $this->db->get('lingkup_kerja');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function insert($data)
    {
        return $this->db->insert('lingkup_kerja', $data);
    }

    public function update($data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update('lingkup_kerja', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('lingkup_kerja');
    }
}