<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_user_detail extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
            return $this->db->get('user_detail')->row_array();
        } else {
            return $this->db->get('user_detail')->result_array();
        }
    }

    public function get_template($pin, $device){
        $this->db->select('b.name as device, a.*');
        $this->db->where('pin', $pin);
        $this->db->where('device_id', $device);
        $this->db->join('device b', 'a.device_id = b.id');
        $result = $this->db->get('user_detail a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_user_detail_device($id){
        $this->db->where('device_id', $id);
        $this->db->order_by('nama');
        $result = $this->db->get('user_detail');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function clear_data($device){
        $this->db->where('device_id', $device);
        return $this->db->delete('user_detail');
    }

    public function insert($data)
    {
        return $this->db->insert('user_detail', $data);
    }

    public function update($data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update('user_detail', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('user_detail');
    }
}
