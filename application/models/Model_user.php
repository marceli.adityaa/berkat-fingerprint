<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_user extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        $this->db->select('a.*, b.jam_kerja, b.lingkup_kerja');
        $this->db->join('user_profile b', 'a.pin = b.pin and a.device_id = b.device_id', 'left');
        if (!empty($id)) {
            $this->db->where('id', $id);
            return $this->db->get('user a')->row_array();
        } else {
            return $this->db->get('user a')->result_array();
        }
    }

    public function get_user_device($id)
    {
        $this->db->select('a.id, a.device_id, a.pin, a.nama, a.is_finger, a.is_rfid, a.status, c.nama as jam_kerja, d.lingkup_kerja');
        $this->db->where('a.device_id', $id);
        $this->db->join('user_profile b', 'a.pin = b.pin and a.device_id = b.device_id', 'left');
        $this->db->join('jam_kerja c', 'b.jam_kerja = c.id', 'left');
        $this->db->join('lingkup_kerja d', 'b.lingkup_kerja = d.id', 'left');
        if(!empty($lingkup = $this->input->get('lingkup')) && $lingkup != 'all'){
            $this->db->where('b.lingkup_kerja', $lingkup);
        }
        if(!empty($jam = $this->input->get('jam')) && $jam != 'all'){
            $this->db->where('b.jam_kerja', $jam);
        }
        $this->db->order_by('a.pin');
        $result = $this->db->get('user a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_active_user_device($id){
        $this->db->where('a.device_id', $id);
        $this->db->where('a.status', 1);
        $this->db->order_by('a.pin');
        $result = $this->db->get('user a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_summary_user_all_device(){
        $this->db->select('IFNULL(COUNT(nama), 0) as total, b.name, b.id');
        $this->db->join('device b', 'a.device_id = b.id', 'right');
        $this->db->where('b.is_active', 1);
        $this->db->order_by('b.name');
        $this->db->group_by('a.device_id');
        $result = $this->db->get('user a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function is_user_exist($pin, $device){
        $this->db->where('pin', $pin);
        $this->db->where('device_id', $device);
        $result = $this->db->get('user');
        if ($result->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function is_user_profile_exist($pin, $device){
        $this->db->where('pin', $pin);
        $this->db->where('device_id', $device);
        $result = $this->db->get('user_profile');
        if ($result->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function clear_data($device)
    {
        $this->db->where('device_id', $device);
        return $this->db->delete('user');
    }

    public function insert($data)
    {
        return $this->db->insert('user', $data);
    }

    
    public function update($data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update('user', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('user');
    }

    public function insert_profile($data)
    {
        return $this->db->insert('user_profile', $data);
    }

    
    public function update_profile($data, $pin, $device)
    {
        $this->db->where('pin', $pin);
        $this->db->where('device_id', $device);
        return $this->db->update('user_profile', $data);
    }
}
