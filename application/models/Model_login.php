<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_login extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
            return $this->db->get('login')->row_array();
        } else {
            return $this->db->get('login')->result_array();
        }
    }

    public function auth($username, $password)
    {
        $result = $this->db->get_where('login', array('username' => $username, 'password' => $password, 'is_active' => 1));
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function insert($data)
    {
        return $this->db->insert('login', $data);
    }

    public function update($data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update('login', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('login');
    }
}
