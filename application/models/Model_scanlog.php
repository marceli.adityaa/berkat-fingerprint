<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_scanlog extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
            return $this->db->get('scanlog')->row_array();
        } else {
            return $this->db->get('scanlog')->result_array();
        }
    }

    public function get_detail($sn, $pin, $date){
        $this->db->select('b.name as device, c.nama as user, DATE_FORMAT(a.scan_date, "%Y-%m-%d") as scan_date, DATE_FORMAT(a.scan_date, "%H:%i") as scan_time');
        $this->db->join('device b', 'a.sn = b.device_sn');
        $this->db->join('user c', 'a.pin = c.pin and c.device_id = b.id');
        $this->db->where('a.pin', $pin);
        $this->db->where('a.sn', $sn);
        $this->db->where('DATE_FORMAT(a.scan_date, "%Y-%m-%d") =', $date);
        $this->db->order_by('a.scan_date');
        $result = $this->db->get('scanlog a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_scanlog_device($id)
    {
        $this->db->select('b.name as device, c.nama as user, DATE_FORMAT(a.scan_date, "%Y-%m-%d") as scan_date, a.pin, DATE_FORMAT(MIN(a.scan_date), "%H:%i") as first_scan, DATE_FORMAT(MAX(a.scan_date), "%H:%i") as last_scan, e.nama as jam_kerja, e.jam_masuk, e.jam_pulang, f.lingkup_kerja');
        $this->db->order_by('scan_date', 'desc');
        $this->db->order_by('a.pin');
        $this->db->join('device b', 'a.sn = b.device_sn');
        $this->db->join('user c', 'a.pin = c.pin and c.device_id = b.id');
        $this->db->join('user_profile d', 'c.pin = d.pin and c.device_id = d.device_id', 'left');
        $this->db->join('jam_kerja e', 'd.jam_kerja = e.id', 'left');
        $this->db->join('lingkup_kerja f', 'd.lingkup_kerja = f.id', 'left');
        if(!empty($_GET['pin']) && $_GET['pin'] != 'all'){
            $this->db->where('a.pin', $_GET['pin']);
        }
        if(!empty($_GET['startdate'])){
            $this->db->where('DATE_FORMAT(a.scan_date, "%Y-%m-%d") >=', $_GET['startdate']);
        }
        if(!empty($_GET['enddate'])){
            $this->db->where('DATE_FORMAT(a.scan_date, "%Y-%m-%d") <=', $_GET['enddate']);
        }
        if(!empty($_GET['lingkup']) && $_GET['lingkup'] != 'all'){
            $this->db->where('d.lingkup_kerja', $_GET['lingkup']);
        }
        $this->db->where('c.device_id', $id);
        $this->db->where('c.status', 1);
        $this->db->group_by('DATE_FORMAT(a.scan_date, "%Y-%m-%d")');
        $this->db->group_by('a.pin');
        $this->db->group_by('a.sn');
        $result = $this->db->get('scanlog a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function clear_data($sn)
    {
        $this->db->where('sn', $sn);
        return $this->db->delete('scanlog');
    }

    public function insert($data)
    {
        return $this->db->insert('scanlog', $data);
    }

    public function update($data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update('scanlog', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('scanlog');
    }
}
