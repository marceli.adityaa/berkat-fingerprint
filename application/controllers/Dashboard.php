<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_user', 'user');
    }

    public function _remap($method, $param = array())
    {
        if (method_exists($this, $method)) {
            if (!empty(get_session('user')['username'])) {
                return call_user_func_array(array($this, $method), $param);
            } else {
                flashdata('info', 'Session Expired.');
                redirect(base_url());
            }
        } else {
            display_404();
        }
    }

    public function index()
    {
        // $this->load->model('model_construction', 'construction');
        set_session('title', 'Dashboard');
        set_session('breadcrumb', array('Home' => 'active'));
        set_activemenu('', 'menu-dashboard');
        $data['device'] = $this->user->get_summary_user_all_device();
        // dd($data);
        init_view('v-dashboard', $data);

    }
}
