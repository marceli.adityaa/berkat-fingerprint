<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_device', 'device');
        $this->load->model('model_user', 'user');
        $this->load->model('model_scanlog', 'scanlog');
    }

    public function _remap($method, $param = array())
    {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $param);
        } else {
            display_404();
        }
    }

    public function download_scanlog()
    {
        $post['sn'] = $this->input->post('sn');
        if(!empty($post['sn'])){
            $detail = $this->db->get_where('device', array('device_sn' => $post['sn']))->row_array();
            echo json_encode(array('status' => 'true', 'message' => 'Processing..'));
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_PORT => $detail['server_port'],
                CURLOPT_URL => "http://" . $detail['server_ip'] . '/scanlog/new',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "sn=" . $post['sn'] . "&limit=",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/x-www-form-urlencoded",
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                $response = ("Error #:" . $err);
            } else {
                $data = json_decode($response);
                $insert_batch = array();
                if ($data->Result == true) {
                    foreach ($data->Data as $row) {
                        $scanlog = array(
                            'sn' => $row->SN,
                            'scan_date' => $row->ScanDate,
                            'pin' => $row->PIN,
                            'verifymode' => $row->VerifyMode,
                            'iomode' => $row->IOMode,
                            'workcode' => $row->WorkCode,
                            'is_sync' => 0,
                        );
                        array_push($insert_batch, $scanlog);
                    }
                    $process = $this->db->insert_batch('scanlog', $insert_batch);
                } else {
                    flashdata("warning", $data->message);
                }
            }

        }else{
            echo json_encode(array('status' => 'false', 'message' => 'Serial Number not found.'));
        }

    }


    public function get_scanlog()
    {
        $post['sn'] = $this->input->get('sn');
        $post['pin'] = $this->input->get('pin');
        $post['date'] = $this->input->get('date');
        $this->db->select('pin, DATE_FORMAT(MIN(scan_date), "%H:%i") as first_scan, IF(DATE_FORMAT(MAX(scan_date), "%H:%i") = DATE_FORMAT(MIN(scan_date), "%H:%i"), "-", DATE_FORMAT(MAX(scan_date), "%H:%i")) as last_scan');
        $this->db->where('DATE_FORMAT(scan_date, "%Y-%m-%d") =', $post['date']);
        $this->db->where('sn', $post['sn']);
        if(!empty($post['pin']) || $post['pin'] == '0'){
            $this->db->where('pin', $post['pin']);
        }
        $this->db->group_by('DATE_FORMAT(scan_date, "%Y-%m-%d")');
        $this->db->group_by('pin');
        $this->db->group_by('sn');
        $this->db->order_by('pin');
        $result = $this->db->get('scanlog');
        $response = array();
        if($result->num_rows() > 0){
            foreach($result->result_array() as $row){
                $response[$row['pin']] = array(
                                            'first_scan' => $row['first_scan'],
                                            'last_scan' => $row['last_scan']
                                        );
            }
        }

        echo json_encode($response);

    }

    public function get_scanlog_detail()
    {
        $post['sn'] = $this->input->get('sn');
        $post['pin'] = $this->input->get('pin');
        $post['date'] = $this->input->get('date');

        # Query select scanlog
        $this->db->select('pin, DATE_FORMAT(scan_date, "%H:%i") as scanlog');
        $this->db->where('DATE_FORMAT(scan_date, "%Y-%m-%d") =', $post['date']);
        $this->db->where('sn', $post['sn']);
        if(!empty($post['pin']) || $post['pin'] == '0'){
            $this->db->where('pin', $post['pin']);
        }
        $this->db->order_by('pin');
        $this->db->order_by('scan_date');
        $result = $this->db->get('scanlog');

        # Query select user
        $this->db->select('pin');
        $this->db->where('DATE_FORMAT(scan_date, "%Y-%m-%d") =', $post['date']);
        $this->db->where('sn', $post['sn']);
        if(!empty($post['pin']) || $post['pin'] == '0'){
            $this->db->where('pin', $post['pin']);
        }
        $this->db->group_by('pin');
        $this->db->order_by('pin');
        $this->db->order_by('scan_date');
        $user = $this->db->get('scanlog');
        $response = array();
        if($result->num_rows() > 0){
            foreach($user->result_array() as $a){
                $log = array();
                foreach($result->result_array() as $row){
                    if($a['pin'] == $row['pin']){
                        array_push($log, $row['scanlog']);
                    }
                }
                $response[$a['pin']] = $log;
            }
        }

        echo json_encode($response);

    }
    
}
