<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Userlogin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_login', 'login');
    }

    public function _remap($method, $param = array())
    {
        if (method_exists($this, $method)) {
            if (!empty(get_session('user')['username'])) {
                return call_user_func_array(array($this, $method), $param);
            } else {
                flashdata('info', 'Session Expired.');
                redirect(base_url());
            }
        } else {
            display_404();
        }
    }

    public function index()
    {
        set_session('title', 'User Login');
        set_session('breadcrumb', array('Home' => base_url('dashboard'), 'User Login' => 'active'));
        set_activemenu('', 'menu-userlogin');
        $data['user'] = $this->login->get();
        init_view('v-userlogin', $data);
    }

    public function submit_form()
    {
        $post = $this->input->post();
        if (empty($post['id'])) {
            # Insert Statement
            $post['password'] = md5($post['password']);
            $post['is_active'] = 1;
            $post['level'] = 1;
            $result = $this->login->insert($post);
            if ($result) {
                flashdata('success', 'Insert Success!');
            } else {
                flashdata('danger', 'Insert Failed!');
            }
        } else {
            # Update Statement
            $id = $post['id'];
            unset($post['id']);
            if (!empty($post['password'])) {
                $post['password'] = md5($post['password']);
            } else {
                unset($post['password']);
            }

            $result = $this->login->update($post, $id);
            if ($result) {
                flashdata('success', 'Update Success!');
            } else {
                flashdata('danger', 'Update Failed!');
            }
        }
        redirect(base_url('userlogin'));
    }

    public function delete_data()
    {
        $id = $this->input->post('id');
        $result = $this->login->delete($id);
        if ($result) {
            flashdata('success', 'Deleted!');
        } else {
            flashdata('danger', 'Failed!');
        }
        echo json_encode($result);
    }

    public function set_active()
    {
        $id = $this->input->post('id');
        $result = $this->login->update(array('is_active' => 1), $id);
        if ($result) {
            flashdata('success', 'Success!');
        } else {
            flashdata('danger', 'Failed!');
        }
        echo json_encode($result);
    }

    public function set_inactive()
    {
        $id = $this->input->post('id');
        $result = $this->login->update(array('is_active' => 0), $id);
        if ($result) {
            flashdata('success', 'Success!');
        } else {
            flashdata('danger', 'Failed!');
        }
        echo json_encode($result);
    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $result = $this->login->get($id);
        echo json_encode($result);
    }
}
