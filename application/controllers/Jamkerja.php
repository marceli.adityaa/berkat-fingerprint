<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jamkerja extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_jamkerja', 'jamkerja');
    }

    public function _remap($method, $param = array())
    {
        if (method_exists($this, $method)) {
            if (!empty(get_session('user')['username'])) {
                return call_user_func_array(array($this, $method), $param);
            } else {
                flashdata('info', 'Session Expired.');
                redirect(base_url());
            }
        } else {
            display_404();
        }
    }

    public function index()
    {
        set_session('title', 'Jam Kerja');
        set_session('breadcrumb', array('Home' => base_url('dashboard'), 'Jam Kerja' => 'active'));
        set_activemenu('', 'menu-jamkerja');
        $data['jamkerja'] = $this->jamkerja->get();
        init_view('v-jamkerja', $data);
    }

    public function submit_form()
    {
        $post = $this->input->post();
        if (empty($post['id'])) {
            # Insert Statement
            $result = $this->jamkerja->insert($post);
            if ($result) {
                flashdata('success', 'Insert Success!');
            } else {
                flashdata('danger', 'Insert Failed!');
            }
        } else {
            # Update Statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->jamkerja->update($post, $id);
            if ($result) {
                flashdata('success', 'Update Success!');
            } else {
                flashdata('danger', 'Update Failed!');
            }
        }
        redirect(base_url('jamkerja'));
    }

    public function delete_data()
    {
        $id = $this->input->post('id');
        if ($this->jamkerja->is_exist($id) == false) {
            $result = $this->jamkerja->delete($id);
            if ($result) {
                flashdata('success', 'Deleted!');
            } else {
                flashdata('danger', 'Failed!');
            }
            echo json_encode($result);
        } else {
            flashdata('danger', 'Can\'t delete, data user exist.');
            echo json_encode('exist');
        }
    }

    public function set_active()
    {
        $id = $this->input->post('id');
        $result = $this->jamkerja->update(array('status' => 1), $id);
        if ($result) {
            flashdata('success', 'Success!');
        } else {
            flashdata('danger', 'Failed!');
        }
        echo json_encode($result);
    }

    public function set_inactive()
    {
        $id = $this->input->post('id');
        $result = $this->jamkerja->update(array('status' => 0), $id);
        if ($result) {
            flashdata('success', 'Success!');
        } else {
            flashdata('danger', 'Failed!');
        }
        echo json_encode($result);
    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $result = $this->jamkerja->get($id);
        echo json_encode($result);
    }
}
