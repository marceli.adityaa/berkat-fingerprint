<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Device extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_device', 'device');
    }

    public function _remap($method, $param = array())
    {
        if (method_exists($this, $method)) {
            if (!empty(get_session('user')['username'])) {
                return call_user_func_array(array($this, $method), $param);
            } else {
                flashdata('info', 'Session Expired.');
                redirect(base_url());
            }
        } else {
            display_404();
        }
    }

    public function index()
    {
        set_session('title', 'device');
        set_session('breadcrumb', array('Home' => base_url('dashboard'), 'device' => 'active'));
        set_activemenu('', 'menu-device');
        $data['device'] = $this->device->get();
        init_view('v-device', $data);
    }

    public function submit_form()
    {
        $post = $this->input->post();
        if (empty($post['id'])) {
            # Insert Statement
            $result = $this->device->insert($post);
            if ($result) {
                flashdata('success', 'Insert Success!');
            } else {
                flashdata('danger', 'Insert Failed!');
            }
        } else {
            # Update Statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->device->update($post, $id);
            if ($result) {
                flashdata('success', 'Update Success!');
            } else {
                flashdata('danger', 'Update Failed!');
            }
        }
        redirect(base_url('device'));
    }

    public function delete_data()
    {
        $id = $this->input->post('id');
        if ($this->device->is_exist($id) == false) {
            $result = $this->device->delete($id);
            if ($result) {
                flashdata('success', 'Deleted!');
            } else {
                flashdata('danger', 'Failed!');
            }
            echo json_encode($result);
        } else {
            flashdata('danger', 'Can\'t delete, data user exist.');
            echo json_encode('exist');
        }
    }

    public function set_active()
    {
        $id = $this->input->post('id');
        $result = $this->device->update(array('is_active' => 1), $id);
        if ($result) {
            flashdata('success', 'Success!');
        } else {
            flashdata('danger', 'Failed!');
        }
        echo json_encode($result);
    }

    public function set_inactive()
    {
        $id = $this->input->post('id');
        $result = $this->device->update(array('is_active' => 0), $id);
        if ($result) {
            flashdata('success', 'Success!');
        } else {
            flashdata('danger', 'Failed!');
        }
        echo json_encode($result);
    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $result = $this->device->get($id);
        echo json_encode($result);
    }

    public function test_connection()
    {
        $id = $this->input->post('id');
        $detail = $this->device->get($id);
        // dd($detail);
        $curl = curl_init();
        set_time_limit(0);
        curl_setopt_array($curl, array(
            CURLOPT_PORT => $detail['server_port'],
            CURLOPT_URL => "http://" . $detail['server_ip'].'/dev/info',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "sn=".$detail['device_sn'],
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        )
        );
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $response = ("Error #:" . $err);
        } else {
            $response;
        }
        echo $response;
    }
}
