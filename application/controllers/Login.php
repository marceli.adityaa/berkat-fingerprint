<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_login', 'login');
    }

    public function _remap($method, $param = array())
    {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $param);
        } else {
            display_404();
        }
	}
	
    public function index()
    {
        $data['captcha'] = captcha();
        set_session('title', 'Login');
        $this->load->view('v-login', $data);
    }

    public function auth(){
        $username = secure($this->input->post('username'));
        $password = md5(secure($this->input->post('password')));
        $auth = $this->login->auth($username, $password);
        if($auth != false){
            # Jika auth berhasil
            $user = array(
                'user_id' => $auth['id'],
                'username' => $auth['username'],
                'level' => $auth['level'],
            );
            $this->session->set_userdata('user', $user);
            $response = array(
                'status' => 1,
                'message' => 'Login success, redirecting..',
                'redirect' => base_url('dashboard')
            );
            flashdata('success', 'Welcome '.ucfirst($user['username']));
        }else{
            # Jika auth gagal
            $response = array(
                'status' => 0,
                'message' => 'Wrong username or password!',
                'redirect' => '',
            );
        }

        echo json_encode($response);
    }
}
