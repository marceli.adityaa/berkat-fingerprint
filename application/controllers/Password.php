<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Password extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_login', 'login');
    }

    public function _remap($method, $param = array())
    {
        if (method_exists($this, $method)) {
            if (!empty(get_session('user')['username'])) {
                return call_user_func_array(array($this, $method), $param);
            } else {
                flashdata('info', 'Session Expired.');
                redirect(base_url());
            }
        } else {
            display_404();
        }
    }

	public function index()
	{
        set_session('title', 'Password');
        set_session('breadcrumb', array('Home' => base_url('dashboard'), 'Password' => 'active'));
        set_activemenu('', '');
        init_view('v-password', $data = array());
	}

	public function submit_form(){
		$username       = get_session('user')['username'];
		$old_password   = md5($this->input->post('old-password'));
		if($this->input->post('new-password') === $this->input->post('retype-new-password')){
			if($this->login->auth($username, $old_password) != false){
				$update['password']	= md5($this->input->post('new-password'));
				$result = $this->login->update($update, get_session('user')['user_id']);
				if($result){
					flashdata('success', 'Success!');
				}else{
					flashdata('danger', 'Failed!');
				}
			}else{
				flashdata('danger', 'Wrong password!');
			}	
		}else{
			flashdata('danger', 'Password not match!');
		}

		redirect(base_url('password'));
	}

	

}
