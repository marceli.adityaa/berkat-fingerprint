<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lingkupkerja extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_lingkupkerja', 'lingkupkerja');
    }

    public function _remap($method, $param = array())
    {
        if (method_exists($this, $method)) {
            if (!empty(get_session('user')['username'])) {
                return call_user_func_array(array($this, $method), $param);
            } else {
                flashdata('info', 'Session Expired.');
                redirect(base_url());
            }
        } else {
            display_404();
        }
    }

    public function index()
    {
        set_session('title', 'Lingkup Kerja');
        set_session('breadcrumb', array('Home' => base_url('dashboard'), 'Lingkup Kerja' => 'active'));
        set_activemenu('', 'menu-lingkupkerja');
        $data['lingkupkerja'] = $this->lingkupkerja->get();
        init_view('v-lingkupkerja', $data);
    }

    public function submit_form()
    {
        $post = $this->input->post();
        if (empty($post['id'])) {
            # Insert Statement
            $result = $this->lingkupkerja->insert($post);
            if ($result) {
                flashdata('success', 'Insert Success!');
            } else {
                flashdata('danger', 'Insert Failed!');
            }
        } else {
            # Update Statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->lingkupkerja->update($post, $id);
            if ($result) {
                flashdata('success', 'Update Success!');
            } else {
                flashdata('danger', 'Update Failed!');
            }
        }
        redirect(base_url('lingkupkerja'));
    }

    public function delete_data()
    {
        $id = $this->input->post('id');
        if ($this->lingkupkerja->is_exist($id) == false) {
            $result = $this->lingkupkerja->delete($id);
            if ($result) {
                flashdata('success', 'Deleted!');
            } else {
                flashdata('danger', 'Failed!');
            }
            echo json_encode($result);
        } else {
            flashdata('danger', 'Can\'t delete, data user exist.');
            echo json_encode('exist');
        }
    }

    public function set_active()
    {
        $id = $this->input->post('id');
        $result = $this->lingkupkerja->update(array('status' => 1), $id);
        if ($result) {
            flashdata('success', 'Success!');
        } else {
            flashdata('danger', 'Failed!');
        }
        echo json_encode($result);
    }

    public function set_inactive()
    {
        $id = $this->input->post('id');
        $result = $this->lingkupkerja->update(array('status' => 0), $id);
        if ($result) {
            flashdata('success', 'Success!');
        } else {
            flashdata('danger', 'Failed!');
        }
        echo json_encode($result);
    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $result = $this->lingkupkerja->get($id);
        echo json_encode($result);
    }
}
