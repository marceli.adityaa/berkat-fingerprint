<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_device', 'device');
        $this->load->model('model_user', 'user');
        $this->load->model('model_jamkerja', 'jamkerja');
        $this->load->model('model_lingkupkerja', 'lingkupkerja');
        $this->load->model('model_user_detail', 'user_detail');
    }

    public function _remap($method, $param = array())
    {
        if (method_exists($this, $method)) {
            if (!empty(get_session('user')['username'])) {
                return call_user_func_array(array($this, $method), $param);
            } else {
                flashdata('info', 'Session Expired.');
                redirect(base_url());
            }
        } else {
            display_404();
        }
    }

    public function index()
    {
        set_session('title', 'User Device');
        set_session('breadcrumb', array('Home' => base_url('dashboard'), 'User Device' => 'active'));
        set_activemenu('', 'menu-user');
        $data['list_device'] = $this->device->get_active_device();
        init_view('v-user-device', $data);
    }

    public function device($id = null)
    {
        if (!empty($id)) {
            set_session('title', 'User');
            $data['device'] = $this->device->get($id);
            $data['jamkerja'] = $this->jamkerja->get_active_jamkerja();
            $data['lingkupkerja'] = $this->lingkupkerja->get_active_lingkupkerja();
            set_session('breadcrumb', array('Home' => base_url('dashboard'), $data['device']['name'] => base_url('user'), 'User' => 'active'));
            set_activemenu('', 'menu-user');
            $data['list_user'] = $this->user->get_user_device($id);
            init_view('v-user', $data);
        }
    }

    public function submit_form()
    {
        $post = $this->input->post();
        if (empty($post['is_finger'])) {
            $post['is_finger'] = 0;
        }
        if (empty($post['is_rfid'])) {
            $post['is_rfid'] = 0;
        }
        $url = $post['url'];
        unset($post['url']);
        if (empty($post['id'])) {
            # Insert Statement
            if ($this->user->is_user_exist($post['pin'], $post['device_id'])) {
                # Jika data sudah ada di database
                flashdata('danger', 'Insert Failed! Data user exist');
            } else {
                # Jika data tidak terdapat di database
                $user = array(
                    'pin' => $post['pin'],
                    'device_id' => $post['device_id'],
                    'nama' => $post['nama'],
                    'is_finger' => $post['is_finger'],
                    'is_rfid' => $post['is_rfid'],
                    'status' => 1,
                );
                $profil = array(
                    'pin' => $post['pin'],
                    'device_id' => $post['device_id'],
                    'jam_kerja' => $post['jam_kerja'],
                    'lingkup_kerja' => $post['lingkup_kerja'],
                );
                $result = $this->user->insert($user);
                if ($this->user->is_user_profile_exist($post['pin'], $post['device_id'])) {
                    # Jika data profile sudah ada di database
                    $result = $this->user->update_profile($profil, $post['pin'], $post['device_id']);
                } else {
                    # Jika tidak
                    $result = $this->user->insert_profile($profil);
                }

                if ($result) {
                    flashdata('success', 'Insert Success!');
                } else {
                    flashdata('danger', 'Insert Failed!');
                }
            }
        } else {
            # Update Statement
            $user = array(
                'nama' => $post['nama'],
                'is_finger' => $post['is_finger'],
                'is_rfid' => $post['is_rfid'],
            );
            $profil = array(
                'pin' => $post['pin'],
                'device_id' => $post['device_id'],
                'jam_kerja' => $post['jam_kerja'],
                'lingkup_kerja' => $post['lingkup_kerja'],
            );
            $result = $this->user->update($user, $post['id']);
            if ($this->user->is_user_profile_exist($post['pin'], $post['device_id'])) {
                # Jika data profile sudah ada di database
                $result = $this->user->update_profile($profil, $post['pin'], $post['device_id']);
            } else {
                # Jika tidak
                $result = $this->user->insert_profile($profil);
            }
            if ($result) {
                flashdata('success', 'Update Success!');
            } else {
                flashdata('danger', 'Update Failed!');
            }
        }
        redirect(base_url('user/device/' . $post['device_id'] . '?' . $url));
    }

    public function delete_data()
    {
        $id = $this->input->post('id');
        if ($this->bank->is_exist($id) == false) {
            $result = $this->bank->delete($id);
            if ($result) {
                flashdata('success', 'Deleted!');
            } else {
                flashdata('danger', 'Failed!');
            }
            echo json_encode($result);
        } else {
            flashdata('danger', 'Can\'t delete, data transaction is exist.');
            echo json_encode('exist');
        }
    }

    public function set_active()
    {
        $id = $this->input->post('id');
        $result = $this->user->update(array('status' => 1), $id);
        if ($result) {
            flashdata('success', 'Success!');
        } else {
            flashdata('danger', 'Failed!');
        }
        echo json_encode($result);
    }

    public function set_inactive()
    {
        $id = $this->input->post('id');
        $result = $this->user->update(array('status' => 0), $id);
        if ($result) {
            flashdata('success', 'Success!');
        } else {
            flashdata('danger', 'Failed!');
        }
        echo json_encode($result);
    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $result = $this->user->get($id);
        echo json_encode($result);
    }

    public function json_get_all_user()
    {
        $id = $this->input->post('id');
        $detail = $this->device->get($id);
        // dd($detail);
        $curl = curl_init();
        set_time_limit(0);
        curl_setopt_array($curl, array(
            CURLOPT_PORT => $detail['server_port'],
            CURLOPT_URL => "http://" . $detail['server_ip'] . '/user/all/paging',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "sn=" . $detail['device_sn'] . "&limit=",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        )
        );
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $response = ("Error #:" . $err);
            dd($response);
        } else {
            $data = json_decode($response);
            $process = true;
            if ($data->Result == 1) {
                $this->user->clear_data($id);
                $this->user_detail->clear_data($id);
                foreach ($data->Data as $row) {
                    $user = array(
                        'pin' => $row->PIN,
                        'device_id' => $id,
                        'nama' => $row->Name,
                        'pwd' => $row->Password,
                        'rfid' => $row->RFID,
                        'privilege' => $row->Privilege,
                    );
                    $insert1 = $this->user->insert($user);
                    if (!$insert1) {
                        $process = false;
                    }
                    foreach ($row->Template as $template) {
                        $detail = array(
                            'pin' => $template->pin,
                            'device_id' => $id,
                            'finger_idx' => $template->idx,
                            'alg_ver' => $template->alg_ver,
                            'template' => $template->template,
                        );
                        $insert2 = $this->user_detail->insert($detail);
                        if (!$insert2) {
                            $process = false;
                        }
                    }
                }
            }
            echo json_encode($process);
        }
    }

    public function get_template()
    {
        $post = $this->input->post();
        $response = $this->user_detail->get_template($post['pin'], $post['device']);
        echo json_encode($response);
    }
}
