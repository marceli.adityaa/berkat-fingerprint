<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Scanlog extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_device', 'device');
        $this->load->model('model_user', 'user');
        $this->load->model('model_scanlog', 'scanlog');
        $this->load->model('model_lingkupkerja', 'lingkupkerja');
    }

    public function _remap($method, $param = array())
    {
        if (method_exists($this, $method)) {
            if (!empty(get_session('user')['username'])) {
                return call_user_func_array(array($this, $method), $param);
            } else {
                flashdata('info', 'Session Expired.');
                redirect(base_url());
            }
        } else {
            display_404();
        }
    }

    public function index()
    {
        set_session('title', 'Scanlog Device');
        set_session('breadcrumb', array('Home' => base_url('dashboard'), 'Scanlog Device' => 'active'));
        set_activemenu('', 'menu-scanlog');
        $data['list_device'] = $this->device->get_active_device();
        init_view('v-scanlog-device', $data);
    }

    public function device($id = null){
        if(!empty($id)){
            $data['device']     = $this->device->get($id); 
            $data['lingkup']    = $this->lingkupkerja->get_active_lingkupkerja();
            $data['list_user']  = $this->user->get_active_user_device($id);
            set_session('title', 'Scanlog');
            set_session('breadcrumb', array('Home' => base_url('dashboard'), $data['device']['name'] => base_url('scanlog'), 'Scanlog' => 'active'));
            set_activemenu('', 'menu-scanlog');
            if(!empty($this->input->get())){
            $data['list_scanlog']  = $this->scanlog->get_scanlog_device($id);
            }else{
                $data['list_scanlog'] = array();
            }
            init_view('v-scanlog', $data);
        }
    }

    public function submit_form()
    {
        $post = $this->input->post();
        if (empty($post['account_id'])) {
            # Insert Statement
            $result = $this->bank->insert(array_map('strtoupper', $post));
            if ($result) {
                flashdata('success', 'Insert Success!');
            } else {
                flashdata('danger', 'Insert Failed!');
            }
        } else {
            # Update Statement
            $account_id = $post['account_id'];
            unset($post['account_id']);
            $result = $this->bank->update(array_map('strtoupper', $post), $account_id);
            if ($result) {
                flashdata('success', 'Update Success!');
            } else {
                flashdata('danger', 'Update Failed!');
            }
        }
        redirect(base_url('bank'));
    }

    public function delete_data()
    {
        $id = $this->input->post('id');
        if ($this->bank->is_exist($id) == false) {
            $result = $this->bank->delete($id);
            if ($result) {
                flashdata('success', 'Deleted!');
            } else {
                flashdata('danger', 'Failed!');
            }
            echo json_encode($result);
        } else {
            flashdata('danger', 'Can\'t delete, data transaction is exist.');
            echo json_encode('exist');
        }
    }

    public function set_active()
    {
        $id = $this->input->post('id');
        $result = $this->bank->update(array('account_status' => 1), $id);
        if ($result) {
            flashdata('success', 'Success!');
        } else {
            flashdata('danger', 'Failed!');
        }
        echo json_encode($result);
    }

    public function set_inactive()
    {
        $id = $this->input->post('id');
        $result = $this->bank->update(array('account_status' => 0), $id);
        if ($result) {
            flashdata('success', 'Success!');
        } else {
            flashdata('danger', 'Failed!');
        }
        echo json_encode($result);
    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $result = $this->bank->get($id);
        echo json_encode($result);
    }

    public function get_new_scanlog(){
        $id = $this->input->post('id');
        $detail = $this->device->get($id);
        $curl = curl_init();
        set_time_limit(0);
        curl_setopt_array($curl, array(
            CURLOPT_PORT => $detail['server_port'],
            CURLOPT_URL => "http://" . $detail['server_ip'].'/scanlog/new',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "sn=".$detail['device_sn']."&limit=",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $process = true;
        if ($err) {
            $response = ("Error #:" . $err);
            dd($response);
        }else{
            $data = json_decode($response);
            $insert_batch = array();
            if($data->Result == true){
                foreach($data->Data as $row){
                    $scanlog = array(
                        'sn' => $row->SN,
                        'scan_date' => $row->ScanDate ,
                        'pin' => $row->PIN,
                        'verifymode' => $row->VerifyMode,
                        'iomode' => $row->IOMode,
                        'workcode' => $row->WorkCode,
                        'is_sync' => 0,
                    );
                    array_push($insert_batch, $scanlog);
                    
                }
                $process = $this->db->insert_batch('scanlog', $insert_batch);
                if($process){
                    flashdata("success", "Success");
                }else{
                    flashdata("error", "Failed");
                }
            }else{
                flashdata("warning", $data->message);
            }
            echo json_encode($process);
        }
    }

    public function get_all_scanlog(){
        $id = $this->input->post('id');
        $detail = $this->device->get($id);
        $session=true;
        $delSession=true;
        $process = true;
        while($session){
            $curl = curl_init();
            set_time_limit(0);
            curl_setopt_array($curl, array(
                CURLOPT_PORT => $detail['server_port'],
                CURLOPT_URL => "http://" . $detail['server_ip'].'/scanlog/all/paging',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "sn=".$detail['device_sn']."&limit=",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/x-www-form-urlencoded",
                ),
            )
            );
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $data = json_decode($response);
            if(($data->Data)>0){	
		
                if($delSession){
                    $querydel = $this->scanlog->clear_data($detail['device_sn']);
                
                    if($querydel){}else{
                        flashdata("error", "Failed");
                    }
                    
                    $delSession=false;
                }
                        
                $batch_insert = array();
                foreach($data->Data as $row){
                    $scanlog = array(
                        'sn' => $row->SN,
                        'scan_date' => $row->ScanDate ,
                        'pin' => $row->PIN,
                        'verifymode' => $row->VerifyMode,
                        'iomode' => $row->IOMode,
                        'workcode' => $row->WorkCode,
                        'is_sync' => 0,
                    );
                    array_push($batch_insert, $scanlog);
                    
                }
                $process = $this->db->insert_batch('scanlog', $batch_insert);
                if($process){
                    flashdata("success", "Success");
                }else{
                    flashdata("error", "Failed");
                }
                    
            }
            
            // if($data->IsSession != $session){
            //     $namafile = "JSOnDataScanLog.txt"; 
            //     $handle = fopen ("..\..\assets/content/".$namafile, "w"); 
            //     if (!$handle) { 				
            //             $server_output = "Filed Save"; 
            //     } else { 				
            //         fwrite ($handle, CreateUserJSON()); 					
            //         $dirname = dirname($path)."/JSOnDataScanLog.txt";
            //     } 
            //     fclose($handle); 
            // }	
            $session=$data->IsSession;            
        }
        echo json_encode($process);
    }

    public function clear_database_scanlog(){
        $id = $this->input->post('id');
        $detail = $this->device->get($id);
        $result = $this->scanlog->clear_data($detail['device_sn']);
        if ($result) {
            flashdata('success', 'Success clear database.');
        } else {
            flashdata('danger', 'Failed!');
        }
        echo json_encode($result);
    }

    public function get_detail_scan(){
        $device = $this->input->post('device');
        $date = $this->input->post('date');
        $pin = $this->input->post('pin');
        $detail = $this->device->get($device);
        $result = $this->scanlog->get_detail($detail['device_sn'], $pin, $date);
        echo json_encode($result);
    }

    public function tes(){
        $start = "08.40";
        $end = "17.30";
        $res = (strtotime($end) - strtotime($start))/60;
        // echo $res.'<br>';
        echo date('D', strtotime('2019-07-06'));
    }
}