<?php
if (!function_exists('monefy')) {
    function monefy($text = "")
    {
        return number_format($text, 0, ',', '.');
    }
}

if (!function_exists('greetings')) {
    function greetings()
    {
        $hour = date('H');
        if ($hour < 15) {
            return ($hour < 12) ? "Selamat Pagi" : "Selamat Siang";
        } else {
            return ($hour > 18) ? "Selamat Malam" : "Selamat Sore";
        }
    }
}

if (!function_exists('trim_empty_data')) {
    function trim_empty_data($data)
    {
        foreach ($data as $index => $item) {
            if (empty($item) && $item !== '0') {
                unset($data[$index]);
            }
        }
        return $data;
    }
}

if (!function_exists('zerofy')) {
    function zerofy($string, $digit = 2)
    {
        return str_pad($string, $digit, '0', STR_PAD_LEFT);
    }
}

if (!function_exists('datify')) {
    function datify($string, $format)
    {
        return date($format, strtotime($string));
    }
}

if (!function_exists('unnullify')) {
    function unnullify($string)
    {
        return ($string === null) ? '' : $string;
    }
}

if (!function_exists('set_session')) {
    function set_session($name, $value)
    {
        $CI = &get_instance();
        $CI->session->set_userdata($name, $value);
    }
}

if (!function_exists('get_session')) {
    function get_session($name)
    {
        $CI = &get_instance();
        return $CI->session->userdata($name);
    }
}

if (!function_exists('set_activemenu')) {
    function set_activemenu($submenu = '', $menu = '')
    {
        $CI = &get_instance();
        $CI->session->set_userdata('submenu', $submenu);
        $CI->session->set_userdata('menu', $menu);
    }
}

if ( ! function_exists('setNewDateTime')){
    function setNewDateTime(){
      date_default_timezone_set("Asia/Jakarta");
      $date = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
      return $date->format('Y-m-d H:i:s');
    }
  }

if (!function_exists('captcha')) {
    function captcha($width = 320, $height = 40)
    {
        $CI = &get_instance();
        $prevCap = $CI->session->userdata('captcha');

        if (!empty($prevCap)) {
            if (file_exists('assets/captcha/' . $prevCap['filename'])) {
                unlink('assets/captcha/' . $prevCap['filename']);
            }

            $CI->session->unset_userdata('captcha');
        }

        $CI->load->helper('captcha');
        $vals = array(
            'word' => rand(1001, 9999),
            'img_path' => './assets/captcha/',
            'img_url' => base_url() . 'assets/captcha/',
            'img_width' => $width,
            'img_height' => $height,
            'expiration' => 7200,
            'font_size' => 16,
            'colors' => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 140, 140)),
        );

        $cap = create_captcha($vals);
        $CI->session->set_userdata('captcha', array('word' => $cap['word'], 'filename' => $cap['filename']));

        return $cap;
    }
}

if (!function_exists('verifikasi_captcha')) {
    function verifikasi_captcha($captcha)
    {
        $CI = &get_instance();
        if ($captcha == $CI->session->userdata('captcha')['word']) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('flashdata')) {
    function flashdata($tipe, $message)
    {
        #tipe pesan
        #1. info
        #2. warning
        #3. danger
        #4. success
        $CI = &get_instance();
        $CI->session->set_flashdata($tipe, $message);
    }
}

if (!function_exists('secure')) {
    function secure($input)
    {
        $input = trim($input);
        $input = htmlentities($input);
        return $input;
    }
}

if (!function_exists('multisecure')) {
    function multisecure($array)
    {
        foreach ($array as $key => $value) {
            $array[$key] = secure($value);
        }
        return $array;
    }
}

if (!function_exists('getDateTime')) {
    function getDateTime()
    {
        date_default_timezone_set("Asia/Jakarta");
        $date = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
        return $date->format('Y-m-d H:i:s');
    }
}

if (!function_exists('getDateNow')) {
    function getDateNow()
    {
        date_default_timezone_set("Asia/Jakarta");
        return date('Y-m-d');
    }
}

if (!function_exists('dd')) {
    function dd($var)
    {
        echo '<pre>';
        print_r($var);
        echo '</pre>';
        die();
    }
}

if (!function_exists('init_view')) {
    function init_view($view, $data = array())
    {
        $CI = &get_instance();
        $CI->load->view('layout/header');
        $CI->load->view('layout/sidebar');
        $CI->load->view($view, $data);
        $CI->load->view('layout/footer');
    }
}

if (!function_exists('display_404')) {
    function display_404()
    {
        $CI = &get_instance();
        $CI->load->view('error-404');
    }
}

if (!function_exists('exportToExcel')) {
    function exportToExcel($data = array(), $title = 'test', $filename = 'test')
    {
        $CI = &get_instance();
        $CI->load->library('excel');
        $objPHPExcel = new Excel();

        // Nama Field Baris Pertama
        $fields = $data->list_fields();
        $col = 0;
        foreach ($fields as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $objPHPExcel->getActiveSheet()->getStyle(chr(65 + $col) . '1:' . chr(65 + $col) . $objPHPExcel->getActiveSheet()->getHighestRow())->getFont()->setBold(true);
            $col++;
        }

        // Isi datanya
        $row = 2;
        foreach ($data->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field)->getColumnDimension(chr(65 + $col))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyle(chr(65 + $col) . $row . ':' . chr(65 + $col) . $objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                $col++;
            }

            $row++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        //Set Title
        $objPHPExcel->getActiveSheet()->setTitle($title);

        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        header('Cache-Control: max-age=0'); //no cache
        //Save ke .xlsx, kalau ingin .xls, ubah 'Excel2007' menjadi 'Excel5'

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
}
