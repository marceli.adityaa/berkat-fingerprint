<div class="form-layout">
    <button type="button" class="btn btn-dark mg-b-10" data-id="<?= $device['id']?>" id="btn-getnewscanlog">Get New Scanlog</button>
    <!-- <button type="button" class="btn btn-danger float-right mg-b-10" data-id="<?= $device['id']?>" id="btn-clear">Clear Database Scanlog</button> -->
    <!-- <button type="button" class="btn btn-warning float-right mg-b-10 mg-r-5" data-id="<?= $device['id']?>" id="btn-getallscanlog">Get All Scanlog</button> -->
    <div id="accordion" class="accordion" role="tablist" aria-multiselectable="true">
        <div class="card">
            <div class="card-header" role="tab" id="headingOne">
                <h6 class="mg-b-0">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                        DEVICE INFO
                    </a>
                </h6>
            </div><!-- card-header -->

            <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-block pd-20">
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Device</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" readonly="" value="<?= ucwords($device['name'])?>">
                        </div>
                    </div>
                    <form method="get" action="<?= base_url('scanlog/device/'.$device['id'].'?')?>">
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">User</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="pin" class="form-control select2">
                                    <option value="all">All</option>
                                    <?php
                                    foreach((array)$list_user as $row){
                                        if(!empty($this->input->get('pin')) && $this->input->get('pin') == $row['pin']){
                                            echo '<option value="'.$row['pin'].'" selected>'.ucwords($row['nama']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['pin'].'">'.ucwords($row['nama']).'</option>';
                                        }
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Lingkup Kerja</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="lingkup" class="form-control">
                                    <option value="all">All</option>
                                    <?php
                                    foreach($lingkup as $row){
                                        if(!empty($this->input->get('lingkup')) && $this->input->get('lingkup') == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.ucwords($row['lingkup_kerja']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.ucwords($row['lingkup_kerja']).'</option>';
                                        }
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Start Date</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="startdate" id="startdate" value="<?= (!empty($_GET['startdate'])) ? $_GET['startdate'] : ''?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">End Date</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0" id="div-enddate">
                                <input type="text" class="form-control" name="enddate" id="enddate" value="<?= (!empty($_GET['enddate'])) ? $_GET['enddate'] : ''?>" autocomplete="off">
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label"></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- card -->
    </div><!-- accordion -->
    <hr>
    <div class="table-responsive mg-t-20">
        <h6 class="card-body-title">Tabel Scanlog</h6>
        <table class="table table-stripped w-100" id="table_scanlog">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>PIN</th>
                    <th>Nama</th>
                    <th>Grup</th>
                    <th>Jam Kerja</th>
                    <th class="text-center">Scan Awal</th>
                    <th class="text-center">Scan Akhir</th>
                    <th class="text-center">Telat</th>
                    <th class="text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($list_scanlog)){
                        foreach($list_scanlog as $row){
                            $telat_first = 0;
                            $telat_last = 0;
                            echo "<tr>";
                            echo "<td nowrap><label class='badge badge-light'>".$row['scan_date']."</label></td>";
                            echo "<td nowrap>".$row['pin']."</td>";
                            echo "<td nowrap><label class='badge badge-light'>".ucwords($row['user'])."</label></td>";
                            if($row['pin'] < 151){
                                echo "<td nowrap><label class='badge badge-primary'>CV. BFI</label><br><label class='badge badge-light'>".ucwords($row['lingkup_kerja'])."</label></td>";
                            }else{
                                echo "<td nowrap><label class='badge badge-info'>PT. BAS</label><br><label class='badge badge-light'>".ucwords($row['lingkup_kerja'])."</label></td>";
                            }
                            echo "<td nowrap><label class='badge badge-light'>".ucwords($row['jam_kerja'])."</label></td>";
                            if(empty($row['jam_masuk'])){
                                $row['jam_masuk'] = "08:00";
                            }

                            if(empty($row['jam_pulang'])){
                                $row['jam_pulang'] = "17:00";
                            }

                            if($row['first_scan'] > $row['jam_masuk']){
                                $telat_first = (strtotime($row['first_scan']) - strtotime($row['jam_masuk']))/60;
                                echo "<td nowrap class='text-center' style='color:red'>".$row['first_scan']."</td>";
                            }else{
                                echo "<td nowrap class='text-center'>".$row['first_scan']."</td>";
                            }
                            if($row['first_scan'] == $row['last_scan']){
                                echo "<td class='text-center'>-</td>";
                                $telat_last = false;
                            }else{
                                if($row['last_scan'] < $row['jam_pulang']){
                                    $telat_last = (strtotime($row['jam_pulang']) - strtotime($row['last_scan']))/60;
                                    echo "<td nowrap class='text-center' style='color:red'>".$row['last_scan']."</td>";
                                }else{
                                    $telat_last = (strtotime($row['jam_pulang']) - strtotime($row['last_scan']))/60;
                                    echo "<td nowrap class='text-center'>".$row['last_scan']."</td>";
                                }
                            }
                            if($telat_first > 0){
                                if($telat_first < 60){
                                    echo "<td nowrap class='text-center'><span class='badge badge-danger'>- ".$telat_first." mins</span></td>";
                                }else{
                                    echo "<td nowrap class='text-center'><span class='badge badge-danger'>- ".date('H.i', mktime(0,$telat_first))." hours</span></td>";
                                }
                            }else{
                                echo "<td nowrap class='text-center'><span class='badge badge-success'>ontime</span></td>";
                            }
                            
                            echo "<td class='text-center'><button type='button' class='btn btn-info btn-detail' data-toggle='tooltip' data-placement='left' title='Detail' data-device='".$device['id']."' data-date='".$row['scan_date']."' data-pin='".$row['pin']."'><i class='fa fa-info-circle'></i></button></td>";
                            echo "</tr>";
                        }
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
<!-- BASIC MODAL -->
<div id="modal_detail" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-y-20 pd-x-25 bg-midnightblack">
                <h6 class="tx-24 mg-b-0 tx-uppercase tx-inverse tx-bold color-brown">DETAIL DATA</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <div class="form-layout form-layout-4">
                    <table class="table table-responsive table-stripped" id="table-detail">
                        <thead>
                            <tr>
                                <th>Device</th>
                                <th>Name</th>
                                <th class="text-center">Scan Date</th>
                                <th class="text-center">Scan Time</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-grey pd-x-20" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->


<link rel="stylesheet" href="<?= base_url()?>assets/jquery-ui-1.12.1/jquery-ui.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/jquery-ui-1.12.1/jquery-ui.theme.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/amanda/lib/datatables/jquery.dataTables.css">
<link rel="stylesheet" href="<?= base_url()?>assets/datatables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/select2/select2.min.css">
<script src="<?= base_url()?>assets/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/amanda/lib/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url()?>assets/amanda/lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="<?= base_url()?>assets/select2/select2.min.js"></script>
<script>
$(document).ready(function() {
    $('#table_scanlog').DataTable({
        'scrollX': true,
        'lengthMenu': [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ]
    });

    $("#startdate, #enddate").datepicker({
        todayHighlight: !0,
        dateFormat: 'yy-mm-dd',
        autoclose: !0,
        maxDate: "<?= date('Y-m-d')?>"
    });

    $('.select2').select2();
});

$('.btn-detail').on('click', function() {
    var device = $(this).data().device;
    var date = $(this).data().date;
    var pin = $(this).data().pin;
    if (device != '' && device != '' && pin != '') {
        $.ajax({
            url: "<?= base_url('scanlog/get_detail_scan')?>",
            type: "POST",
            dataType: "json",
            data: {
                'pin': pin,
                'device': device,
                'date': date,
            },
            success: function(result) {
                if (result != false) {
                    $('#table-detail > tbody').empty();
                    $.each(result, function(key, val) {
                        $("#table-detail").find('tbody').append('\
                            <tr><td>' + val.device + '</td>\
							<td>' + val.user + '</td>\
							<td class="text-center">' + val.scan_date + '</td>\
							<td class="text-center">' + val.scan_time + '</td>\
							</tr>');
                    });
                    $('#modal_detail').modal('show');
                } else {
                    swal.fire("Info", "Gagal mengambil data", "warning");
                }
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
});


$('#btn-getnewscanlog').on('click', function() {
    var id = $(this).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('scanlog/get_new_scanlog')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                swal.fire('Failed', e.responseText, 'error');
                console.log(e);
            },
            complete: function(e) {}
        });
    }
});

$('#btn-getallscanlog').on('click', function() {
    var id = $(this).data().id;
    if (id != '') {
        Swal.fire({
            title: 'Konfirmasi?',
            text: "Semua data scanlog akan diambil ulang, lanjutkan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('scanlog/get_all_scanlog')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        swal.fire('Failed', e.responseText, 'error');
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
});

$('#btn-clear').on('click', function() {
    var id = $(this).data().id;
    if (id != '') {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Database scanlog akan direset ulang, lanjutkan?",
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('scanlog/clear_database_scanlog')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        swal.fire('Failed', e.responseText, 'error');
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
});
</script>