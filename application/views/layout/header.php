<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Menjadi Perusahaan yang berkembang besar dan dapat menjadi berkat bagi orang lain melalui kualitas yang baik dari setiap produk yang disajikan.">
    <meta name="author" content="Berkat Group">


    <title>Berkat Fingerprint | <?=(!empty(get_session('title')) ? ucwords(get_session('title')) : 'Dashboard')?></title>

    <!-- vendor css -->
    <link href="<?= base_url()?>assets/amanda/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/amanda/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/amanda/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/amanda/lib/jquery-toggles/toggles-full.css" rel="stylesheet">

    <!-- Amanda CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets/amanda/css/amanda.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/mystyle.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/sweetalert2/borderless.min.css">
    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url('assets/img/logo/small/berkat-group.png')?>" />
    <script src="<?= base_url()?>assets/amanda/lib/jquery/jquery.js"></script>
</head>

<body>
    <div class="preloader">
        <div class="loader">
            <img src="<?= base_url('assets/img/spinner.gif')?>" class="img-fluid">
        </div>
    </div>

    <div class="ajaxloader" style="display:none;">
        <div class="loader">
            <img src="<?= base_url('assets/img/spinner2.gif')?>" class="img-fluid"> Loading...
        </div>
    </div>
