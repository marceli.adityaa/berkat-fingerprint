</div>
</div>
</div><!-- am-mainpanel -->
<script src="<?= base_url()?>assets/amanda/lib/popper.js/popper.js"></script>
<script src="<?= base_url()?>assets/amanda/lib/bootstrap/bootstrap.js"></script>
<script src="<?= base_url()?>assets/amanda/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="<?= base_url()?>assets/amanda/lib/jquery-toggles/toggles.min.js"></script>
<script src="<?= base_url()?>assets/amanda/js/amanda.js"></script>
<script src="<?= base_url()?>assets/amanda/js/ResizeSensor.js"></script>
<script src="<?= base_url()?>assets/sweetalert2/dist/sweetalert2.all.min.js"></script>
<script src="<?= base_url()?>assets/myscript.js"></script>
<script>
$(document).ready(function() {
    $(document).ajaxStart(function() {
        $(".ajaxloader").fadeIn();
    });
    $(document).ajaxComplete(function() {
        $(".ajaxloader").fadeOut();
    });

    $("form").on("submit", function(e) {
        loading_button('.btn-submit');
        $('.btn-submit').attr('disabled', true);
    });

    $('[name=username]').focus();
    var info = "<?= get_session('info');?>";
    var danger = "<?= get_session('danger');?>";
    var success = "<?= get_session('success');?>";
    var warning = "<?= get_session('warning');?>";
    if (info != '') {
        show_alert("info", info);
    }
    if (danger != '') {
        show_alert("error", danger);
    }
    if (success != '') {
        show_alert("success", success);
    }
    if (warning != '') {
        show_alert("warning", warning);
    }
    $('[data-toggle="tooltip"]').tooltip();
});

$('.numeric').keyup(function(e) {
    if (/\D/g.test(this.value)) {
        this.value = this.value.replace(/\D/g, '');
    }
});

function reload_page() {
    window.location.reload();
}

// Script pre-loader page
$(window).load(function() {
    $(".preloader").fadeOut("slow");;
});

// Script toggle active submenu dan menu
var submenu = "<?= get_session('submenu')?>";
var menu = "<?= get_session('menu')?>";

if (submenu != '') {
    $("." + submenu).next().slideToggle();
    $("." + submenu).toggleClass('show-sub');
    $("." + submenu).toggleClass('active');
}

if (menu != '') {
    $("." + menu).toggleClass('active');
}



function show_alert(type, message) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-right',
        showConfirmButton: false,
        timer: 3000
    });

    Toast.fire({
        type: type,
        title: message
    })
}

function loading_button(element) {
    var $this = $(element);
    $this.data('original-text', $(element).html());
    $this.html('<i class="fa fa-circle-o-notch fa-spin"></i> loading..');
}
</script>
</body>

</html>