<div class="am-header bg-midnightblack">
    <div class="am-header-left">
        <a id="naviconLeft" href="" class="am-navicon d-none d-lg-flex"><i class="icon ion-navicon-round"></i></a>
        <a id="naviconLeftMobile" href="" class="am-navicon d-lg-none"><i class="icon ion-navicon-round"></i></a>
        <a href="#" class="am-logo"><img src="<?=base_url('assets/img/logo/berkat-group-horizontal.png')?>" class="img-fluid wd-40p"></a>
    </div><!-- am-header-left -->

    <div class="am-header-right">
        <div class="dropdown dropdown-profile">
            <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                <img src="assets/img/blank_user2.png" class="wd-32 rounded-circle" alt="">
                <span class="logged-name"><span class="hidden-xs-down color-brown"><?=!empty(get_session('user')['username']) ? ucwords(get_session('user')['username']) : 'User'?></span> <i class="fa fa-angle-down mg-l-3"></i></span>
            </a>
            <div class="dropdown-menu wd-200">
                <ul class="list-unstyled user-profile-nav">
                    <li><a href="<?= base_url('password')?>"><i class="icon fa fa-lock"></i> Change Password</a></li>
                    <li><a href="<?= base_url('logout')?>"><i class="icon ion-power"></i> Logout</a></li>
                </ul>
            </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
    </div><!-- am-header-right -->
</div><!-- am-header -->

<div class="am-sideleft">
    <div class="tab-content">
        <div id="mainMenu" class="tab-pane active">
            <ul class="nav am-sideleft-menu">
                <li class="nav-item">
                    <a href="<?=base_url('dashboard')?>" class="nav-link menu-dashboard">
                        <i class="icon fa fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li><!-- nav-item -->
                <li class="nav-item">
                    <a href="<?=base_url('userlogin')?>" class="nav-link menu-userlogin">
                        <i class="icon fa fa-user"></i>
                        <span>Master Login</span>
                    </a>
                </li><!-- nav-item -->
                <li class="nav-item">
                    <a href="<?=base_url('jamkerja')?>" class="nav-link menu-jamkerja">
                        <i class="icon fa fa-clock-o"></i>
                        <span>Master Jam Kerja</span>
                    </a>
                </li><!-- nav-item -->
                <li class="nav-item">
                    <a href="<?=base_url('lingkupkerja')?>" class="nav-link menu-lingkupkerja">
                        <i class="icon fa fa-map-pin"></i>
                        <span>Master Lingkup Kerja</span>
                    </a>
                </li><!-- nav-item -->
                <li class="nav-item">
                    <a href="<?=base_url('device')?>" class="nav-link menu-device">
                        <i class="icon fa fa-mobile-phone"></i>
                        <span>Master Device</span>
                    </a>
                </li><!-- nav-item -->
                <li class="nav-item">
                    <a href="<?=base_url('user')?>" class="nav-link menu-user">
                        <i class="icon fa fa-address-book-o"></i>
                        <span>User</span>
                    </a>
                </li><!-- nav-item -->
                <li class="nav-item">
                    <a href="<?=base_url('scanlog')?>" class="nav-link menu-scanlog">
                        <i class="icon fa fa-search"></i>
                        <span>Scanlog</span>
                    </a>
                </li><!-- nav-item -->
            </ul>
        </div><!-- #mainMenu -->
    </div><!-- tab-content -->
</div><!-- am-sideleft -->
<div class="am-mainpanel">
    <div class="am-pagebody">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <?php 
                    $breadcrumb = get_session('breadcrumb');
                    foreach($breadcrumb as $key => $val){
                        if($val != 'active'){
                            echo '<li class="breadcrumb-item"><a href="'.$val.'">'.ucfirst($key).'</a></li>';
                        }else{
                            echo '<li class="breadcrumb-item active" aria-current="page">'.ucfirst($key).'</li>';
                        }
                    }
                ?>
            </ol>
        </nav>
        <div class="card card-body">