<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Menjadi Perusahaan yang berkembang besar dan dapat menjadi berkat bagi orang lain melalui kualitas yang baik dari setiap produk yang disajikan.">
    <meta name="author" content="Berkat Group">

    <title>Berkat Group - Fingerprint | <?=(!empty(get_session('title')) ? get_session('title') : 'Dashboard')?></title>

    <!-- vendor css -->
    <!-- <link href="assets/amanda/lib/Ionicons/css/ionicons.css" rel="stylesheet"> -->
    <!-- <link href="assets/amanda/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet"> -->
    <link href="<?= base_url()?>assets/amanda/lib/jquery-toggles/toggles-full.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/amanda/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- Amanda CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets/amanda/css/amanda.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/mystyle.css">
    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url('assets/img/logo/small/berkat-group.png')?>" />
</head>

<body>
    <div class="preloader">
        <div class="loader">
            <img src="<?= base_url('assets/img/spinner.gif')?>" class="img-fluid">
        </div>
    </div>
    <div class="am-signin-wrapper bg-midnightblack">
        <div class="am-signin-box">
            <div class="row no-gutters">
                <div class="col-lg-5 bg-browngold">
                    <div>
                        <img src="<?=base_url('assets/img/logo/berkat-group-white.png')?>" class="img-fluid" alt="Berkat Group">
                    </div>
                </div>
                <div class="col-lg-7">
                    <form id="form-login">
                        <h5 class="tx-gray-800 mg-b-25">Berkat Group - <b class="tx-bold ">Fingerprint</b></h5>

                        <div class="form-group">
                            <label class="form-control-label">Username:</label>
                            <input type="text" name="username" class="form-control form-input" placeholder="Enter your username" autocomplete="off">
                        </div><!-- form-group -->

                        <div class="form-group">
                            <label class="form-control-label">Password:</label>
                            <input type="password" name="password" class="form-control form-input" placeholder="Enter your password">
                        </div><!-- form-group -->

                        <div class="form-group mg-b-20">
                            <img class="img-fluid" src="<?=base_url('assets/captcha/' . $captcha['filename'])?>"><br>
                            <input type="text" class="form-control form-input mg-t-10" name="captcha" placeholder="Retype the characters from picture" autocomplete="off">
                        </div>

                        <button type="button" class="btn btn-block bg-midnightblack" id="btn-login">Login</button>
                    </form>
                </div><!-- col-7 -->
            </div><!-- row -->
            <p class="tx-center tx-white-5 tx-12 mg-t-25">Copyright &copy; 2019. Berkat Group. <br>All Rights Reserved.
            </p>
        </div><!-- signin-box -->
    </div><!-- am-signin-wrapper -->

    <script src="<?= base_url()?>assets/amanda/lib/jquery/jquery.js"></script>
    <script src="<?= base_url()?>assets/amanda/lib/popper.js/popper.js"></script>
    <script src="<?= base_url()?>assets/amanda/lib/bootstrap/bootstrap.js"></script>
    <script src="<?= base_url()?>assets/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script>
    $(document).ready(function() {
        // $(document).ajaxStart(function() {
        //     $(".preloader").fadeIn("slow");
        // });
        // $(document).ajaxComplete(function() {
        //     $(".preloader").fadeOut("slow");
        // });
        $('[name=username]').focus();
        var info = "<?= get_session('info')?>";
        if (info != '') {
            show_alert('default', info);
        }
    });

    function reload_page() {
        window.location.reload();
    }

    $('.form-input').on('keypress', function(e) {
        if (e.which === 13) {
            auth_login();
        }
    });

    // Script pre-loader page
    $(window).load(function() {
        $(".preloader").fadeOut("slow");
    });

    // Script action login
    $('#btn-login').on('click', function(e) {
        auth_login();
    });

    function auth_login() {
        var username = $('[name=username]').val();
        var password = $('[name=password]').val();
        var captcha = $('[name=captcha]').val();
        var verif = "<?= get_session('captcha')['word']?>";
        if (username != '' && password != '' && captcha != '') {
            if (captcha === verif) {
                loading_button('#btn-login');
                $('#btn-login').attr('disabled', true);
                $.ajax({
                    url: "<?= base_url('login/auth')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'username': username,
                        'password': password
                    },
                    success: function(result) {
                        if (result.status == 1) {
                            // Success
                            show_alert('success', result.message);
                            window.location.href = result.redirect;
                        } else {
                            // Failed
                            show_alert('error', result.message);
                            $("#form-login").trigger('reset');
                            $("#btn-login").html('Login');
                            $("[name=username]").focus();
                        }
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {
                        $('#btn-login').attr('disabled', false);
                    }
                });
            } else {
                show_alert('error', 'Wrong captcha');
                $('[name=captcha]').val('').focus();
            }
        } else {
            if (username == '') {
                $('[name=username]').focus();
            } else if (password == '') {
                $('[name=password]').focus();
            } else if (captcha == '') {
                $('[name=captcha]').focus();
            }
        }
    }

    function loading_button(element) {
        var $this = $(element);
        $this.data('original-text', $(element).html());
        $this.html('<i class="fa fa-circle-o-notch fa-spin"></i> loading..');
    }

    // Script alert
    function show_alert(type, message) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'bottom-right',
            showConfirmButton: false,
            timer: 3000
        });

        Toast.fire({
            type: type,
            title: message
        })
    }
    </script>

</body>

</html>