<div class="form-layout">
    <button class="btn btn-dark" type="button" id="btn_add"><i class="fa fa-plus"></i> Add Data</button>
    <div class="table-responsive mg-t-20">
        <h6 class="card-body-title">Table Jam Kerja</h6>
        <table class="table table-stripped w-100" id="table_jamkerja">
            <thead>
                <tr>
                    <th class='text-center'>No</th>
                    <th>Nama Profil</th>
                    <th>Jam Masuk</th>
                    <th>Jam Pulang</th>
                    <th class='text-center'>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($jamkerja)){
                        foreach($jamkerja as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td nowrap>".$row['nama']."</td>";
                            echo "<td nowrap>".$row['jam_masuk']."</td>";
                            echo "<td nowrap>".$row['jam_pulang']."</td>";
                            if($row['status'] == 1){
                                echo "<td class='text-center'><label class='badge badge-success'>Active</label></td>";
                            }else{
                                echo "<td class='text-center'><label class='badge badge-secondary'>Inactive</label></td>";
                            }
                            echo "<td nowrap>
                            <button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='left' title='Edit Data' onclick='edit(".$row['id'].")'><i class='fa fa-edit'></i></button>";
                            if($row['status'] == 0){
                                echo " <button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='left' title='Set to Active' onclick='active(".$row['id'].")'><i class='fa fa-check'></i></button>";
                            }else{
                                echo " <button type='button' class='btn btn-secondary' data-toggle='tooltip' data-placement='left' title='Set to Inactive' onclick='inactive(".$row['id'].")'><i class='fa fa-times'></i></button>";
                            }
                            // echo " <button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='left' title='Delete Data' onclick='delete_data(".$row['id'].")'><i class='fa fa-trash'></i></button></td>";
                            echo "</tr>";
                        }
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
<!-- BASIC MODAL -->
<div id="modal_form" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content bd-0 tx-14">
            <form method="post" action="<?= base_url('jamkerja/submit_form')?>">
                <div class="modal-header pd-y-20 pd-x-25 bg-midnightblack">
                    <h6 class="tx-24 mg-b-0 tx-uppercase tx-inverse tx-bold color-brown">FORM DATA</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <div class="form-layout form-layout-4">
                        <input type="hidden" name="id" id="id" value="">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Nama Profil <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="nama" class="form-control" placeholder="Enter profile name" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Jam Masuk</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="time" name="jam_masuk" class="form-control" placeholder="Enter jam masuk" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Jam Pulang</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="time" name="jam_pulang" class="form-control" placeholder="Enter jam pulang" autocomplete="off" required="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-dark pd-x-20 btn-submit">Submit</button>
                    <button type="button" class="btn btn-grey pd-x-20" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->
<link rel="stylesheet" href="<?= base_url()?>assets/jquery-ui-1.12.1/jquery-ui.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/jquery-ui-1.12.1/jquery-ui.theme.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/amanda/lib/datatables/jquery.dataTables.css">
<link rel="stylesheet" href="<?= base_url()?>assets/datatables/css/dataTables.bootstrap4.min.css">
<script src="<?= base_url()?>assets/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/amanda/lib/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url()?>assets/amanda/lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
$(document).ready(function() {
    $('#table_jamkerja').DataTable({
        'scrollX': true,
        'lengthMenu': [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ]
    });
});

$('#btn_add').on('click', function() {
    $('form').trigger('reset');
    $('[name=id]').val('');
    $('#modal_form').modal('show');
});

function active(id) {
    if (id != '') {
        $.ajax({
            url: "<?= base_url('jamkerja/set_active')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function edit(id) {
    if (id != '') {
        $.ajax({
            url: "<?= base_url('jamkerja/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('[name=id]').val(result.id);
                $('[name=nama]').val(result.nama);
                $('[name=jam_masuk]').val(result.jam_masuk);
                $('[name=jam_pulang]').val(result.jam_pulang);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function inactive(id) {
    if (id != '') {
        $.ajax({
            url: "<?= base_url('jamkerja/set_inactive')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function delete_data(id) {
    if (id != '') {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('jamkerja/delete_data')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
}
</script>