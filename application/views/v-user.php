<div class="form-layout">
    <!-- <button type="button" class="btn btn-dark mg-b-10" data-id="<?= $device['id']?>" id="btn-getalluser">Get All User</button> -->
    <button class="btn btn-dark" type="button" id="btn_add"><i class="fa fa-plus"></i> Add User</button>
    <div id="accordion" class="accordion mg-t-10" role="tablist" aria-multiselectable="true">
        <div class="card">
            <div class="card-header" role="tab" id="headingOne">
                <h6 class="mg-b-0">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                        DEVICE INFO
                    </a>
                </h6>
            </div><!-- card-header -->

            <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-block pd-20">
                    <form method="get" action="<?= base_url('user/device/'.$device['id'].'?')?>">
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Name</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= ucwords($device['name'])?>">
                            </div>
                        </div>
                        <!-- <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Serial Number</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= ucwords($device['device_sn'])?>">
                            </div>
                        </div> -->
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Lingkup Kerja</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="lingkup" class="form-control">
                                    <option value="all">All</option>
                                    <?php 
                                        foreach($lingkupkerja as $row){
                                            if($row['id'] == $this->input->get('lingkup')){
                                                echo "<option value='".$row['id']."' selected>".$row['lingkup_kerja']."</option>";
                                            }else{
                                                echo "<option value='".$row['id']."'>".$row['lingkup_kerja']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Jam Kerja</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="jam" class="form-control">
                                    <option value="all">All</option>
                                    <?php 
                                        foreach($jamkerja as $row){
                                            if($row['id'] == $this->input->get('jam')){
                                                echo "<option value='".$row['id']."' selected>".$row['nama']."</option>";
                                            }else{
                                                echo "<option value='".$row['id']."'>".$row['nama']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label"></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search"></i> Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- card -->
    </div><!-- accordion -->
    <hr>
    <div class="table-responsive mg-t-20">
        <h6 class="card-body-title">Table User</h6>
        <table class="table table-stripped w-100" id="table_supplier">
            <thead>
                <tr>
                    <th class='text-center'>No</th>
                    <th>PIN</th>
                    <th>Nama</th>
                    <th>Jam Kerja</th>
                    <th>Lingkup Kerja</th>
                    <th class='text-center'>Finger</th>
                    <th class='text-center'>Kartu</th>
                    <th class='text-center'>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($list_user)){
                        foreach($list_user as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td nowrap>".$row['pin']."</td>";
                            echo "<td nowrap>".ucwords($row['nama'])."</td>";
                            echo "<td nowrap>".$row['jam_kerja']."</td>";
                            echo "<td nowrap>".$row['lingkup_kerja']."</td>";
                            if($row['is_finger']){
                                echo "<td class='text-center'><label class='badge badge-success'>Ya</label></td>";
                            }else{
                                echo "<td class='text-center'><label class='badge badge-secondary'>Tidak</label></td>";
                            }
                            if($row['is_rfid']){
                                echo "<td class='text-center'><label class='badge badge-success'>Ya</label></td>";
                            }else{
                                echo "<td class='text-center'><label class='badge badge-secondary'>Tidak</label></td>";
                            }
                            if($row['status']){
                                echo "<td class='text-center'><label class='badge badge-success'>Aktif</label></td>";
                            }else{
                                echo "<td class='text-center'><label class='badge badge-secondary'>Nonaktif</label></td>";
                            }
                            echo "<td nowrap>
                            <button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='left' title='Edit Data' onclick='edit(".$row['id'].")'><i class='fa fa-edit'></i></button>";
                            if($row['status'] == 0){
                                echo " <button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='left' title='Set to Active' onclick='active(".$row['id'].")'><i class='fa fa-check'></i></button>";
                            }else{
                                echo " <button type='button' class='btn btn-secondary' data-toggle='tooltip' data-placement='left' title='Set to Inactive' onclick='inactive(".$row['id'].")'><i class='fa fa-times'></i></button>";
                            }
                            echo "</tr>";
                        }
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
<!-- BASIC MODAL -->
<div id="modal_form" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content bd-0 tx-14">
            <form method="post" action="<?= base_url('user/submit_form')?>">
                <div class="modal-header pd-y-20 pd-x-25 bg-midnightblack">
                    <h6 class="tx-24 mg-b-0 tx-uppercase tx-inverse tx-bold color-brown">FORM DATA</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <div class="form-layout form-layout-4">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="device_id" id="device_id" value="">
                        <input type="hidden" name="url" id="url" value="<?= $_SERVER['QUERY_STRING']?>">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">PIN <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="pin" class="form-control numeric" placeholder="" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Nama <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="nama" class="form-control" placeholder="" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Jam Kerja <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select class="form-control" name="jam_kerja" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    <?php 
                                        foreach($jamkerja as $row){
                                            echo "<option value='".$row['id']."'>".$row['nama']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Lingkup Kerja <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select class="form-control" name="lingkup_kerja" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    <?php 
                                        foreach($lingkupkerja as $row){
                                            echo "<option value='".$row['id']."'>".$row['lingkup_kerja']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Jenis Identitas <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <label class='ckbox'>
                                    <input type='checkbox' name='is_finger' value='1'><span>Finger</span>
                                </label>
                                <label class='ckbox mg-l-10'>
                                    <input type='checkbox' name='is_rfid' value='1'><span>Kartu</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-dark pd-x-20 btn-submit">Submit</button>
                    <button type="button" class="btn btn-grey pd-x-20" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->

<link rel="stylesheet" href="<?= base_url()?>assets/jquery-ui-1.12.1/jquery-ui.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/jquery-ui-1.12.1/jquery-ui.theme.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/amanda/lib/datatables/jquery.dataTables.css">
<link rel="stylesheet" href="<?= base_url()?>assets/datatables/css/dataTables.bootstrap4.min.css">
<script src="<?= base_url()?>assets/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/amanda/lib/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url()?>assets/amanda/lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
$(document).ready(function() {
    $('#table_supplier').DataTable({
        'scrollX': true,
        'lengthMenu': [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ]
    });
});

$('#btn_add').on('click', function() {
    init_form();
    $('#modal_form').modal('show');
});

function init_form() {
    $('#modal_form form').trigger('reset');
    $('#modal_form [name=id]').val('');
    $('#modal_form [name=device_id]').val("<?= $device['id']?>");
    $('#url').val("<?= $_SERVER['QUERY_STRING']?>");
}

function active(id) {
    if (id != '') {
        $.ajax({
            url: "<?= base_url('user/set_active')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function inactive(id) {
    if (id != '') {
        $.ajax({
            url: "<?= base_url('user/set_inactive')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function edit(id) {
    if (id != '') {
        $.ajax({
            url: "<?= base_url('user/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#modal_form [name=id]').val(result.id);
                $('#url').val("<?= $_SERVER['QUERY_STRING']?>");
                $('#modal_form [name=device_id]').val(result.device_id);
                $('#modal_form [name=pin]').val(result.pin);
                $('#modal_form [name=nama]').val(result.nama);
                $('#modal_form [name=jam_kerja]').val(result.jam_kerja).change();
                $('#modal_form [name=lingkup_kerja]').val(result.lingkup_kerja).change();
                if(result.is_finger == 1){
                    $('#modal_form [name=is_finger]').prop('checked', true);
                }else{
                    $('#modal_form [name=is_finger]').prop('checked', false);
                }
                if(result.is_rfid == 1){
                    $('#modal_form [name=is_rfid]').prop('checked', true);
                }else{
                    $('#modal_form [name=is_rfid]').prop('checked', false);
                }
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}
</script>