<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Berkat Group | 404 - Page Not Found</title>

    <!-- vendor css -->
    <link href="<?= base_url()?>assets/amanda/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/amanda/lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Amanda CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets/amanda/css/amanda.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/mystyle.css">
    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url('assets/img/logo/small/berkat-group.png')?>" />
</head>

<body>

    <div class="ht-100v d-flex align-items-center justify-content-center bg-midnightblack">
        <div class="wd-lg-70p wd-xl-50p tx-center pd-x-40">
            <img class="img-fluid wd-35p mg-b-20" src="<?= base_url('assets/img/logo/berkat-group.png')?>">
            <h1 class="tx-80 tx-xs-100 tx-normal tx-white-6 mg-b-0">404!</h1>
            <h5 class="tx-xs-24 tx-normal tx-orange mg-b-10 lh-5">The page your are looking for has not been found.</h5>
            <p class="tx-16 mg-b-30 tx-white-6">The page you are looking for might have been removed, had its name changed,
                or unavailable.</p>

            

                <a onclick="history.go(-1);" class="tx-orange hover-info tx-16 btn btn-warning active">Go Back</a>
            
        </div>
    </div><!-- ht-100v -->
</body>

</html>