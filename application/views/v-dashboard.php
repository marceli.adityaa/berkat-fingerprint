<div class="row">
    <?php 
    foreach($device as $row){
        echo '
        <div class="col">
            <div class="card bd-0">
                <div class="card-header card-header-default bg-browngold">
                    Device : &nbsp;<b>'.ucwords($row['name']).'</b>
                </div>
                <div class="card-body bd bd-t-0">
                    <h4 class="text-center">Total User</h4>
                    <h1 class="text-center tx-80 mg-t-20 tx-black">'.$row['total'].'</h1>
                </div>
            </div>
        </div>
        ';
    }
    ?>


</div>

<script src="<?= base_url()?>assets/highcharts/highcharts.js"></script>
<script src="<?= base_url()?>assets/highcharts/exporting.js"></script>
<script src="<?= base_url()?>assets/highcharts/export-data.js"></script>
<script>

</script>