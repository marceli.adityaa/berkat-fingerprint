<div class="form-layout">
    <div class="table-responsive mg-t-20">
        <h6 class="card-body-title">Table Device</h6>
        <table class="table table-stripped w-100" id="table_supplier">
            <thead>
                <tr>
                    <th class='text-center'>No</th>
                    <th>Name</th>
                    <th>Serial Number</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($list_device)){
                        foreach($list_device as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td nowrap>".$row['name']."</td>";
                            echo "<td nowrap>".$row['device_sn']."</td>";
                            echo "<td nowrap><a href='".base_url('user/device/'.$row['id'])."'><button type='button' class='btn btn-dark color-brown' data-toggle='tooltip' data-placement='left' title='Pilih Device'><i class='fa fa-play'></i></button></a></td>";
                            echo "</tr>";
                        }
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>

<link rel="stylesheet" href="<?= base_url()?>assets/jquery-ui-1.12.1/jquery-ui.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/jquery-ui-1.12.1/jquery-ui.theme.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/amanda/lib/datatables/jquery.dataTables.css">
<link rel="stylesheet" href="<?= base_url()?>assets/datatables/css/dataTables.bootstrap4.min.css">
<script src="<?= base_url()?>assets/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/amanda/lib/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url()?>assets/amanda/lib/datatables-responsive/dataTables.responsive.js"></script>
<script>
$(document).ready(function() {
    $('#table_supplier').DataTable({
        'scrollX': true,
        'lengthMenu': [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ]
    });
});
</script>